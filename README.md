# notes_for_students
A collection of random thoughts potentially useful for students


## Academic life
* You are smart. That is why your professor choose you. When you don't know how to do something don't think "I don't know how to do this!". Think instead "if someone were to solve this what would they need to know?". Then figure out how to get that knowledge you miss. That will lead you to another issue to figure out. That is research.
* If a good number of your ideas don't turn out to be bad ideas you are not having enough ideas. Having good ideas is about having many ideas where some turn out to be good. REF?
* Before you believe any graph internalise what they teach here: https://www.youtube.com/watch?v=A2OtU5vlR0k&index=1&list=PLPnZfvKID1Sje5jWxt-4CSZD7bUI4gSPS
* If you meet people that seem to believe they understand everything it usually means they don't have more than a superficial knowlegde of anything.
* The people that are always oh so busy are usually just not well organized. It doesn't mean they get more done than you. Don't think that it means they are getting much more done than you. It usually doesn't. This doesn't translate to other with very different types of jobs than you though. After the first postdoc you get many more diverse responsibilities and the number of tasks do increase significantly.
* Many studies show that there does not exist people that are good at multi-tasking. It is not efficient for anyone. Fight hard to avoid it when you plan your work. One thing at a time, finish, next.
* Understand your mandate. Are you in charge of the budget? Who is? Is your project part of a bigger project or a single project? How much money can you spend without asking your professor? What mandate do you have when talking to others regarding collaborations and other things you could spend your time on? This is especially important before you go alone to a project meeting etc. Ask your professor all this.

## Managing your professor and supervisors
* It is YOUR project (if your supervisor is decent). Take charge. Say what YOU want to do. If you think what the supervisor suggest doesn't make sense say so right now. He/she might not have all the pieces you have. A good supervisor wants you to take charge and follow your own ideas. In fact he/she has been waiting for you to do that since you started. It is a sign that you are getting to be an independent reseacher.
* No, your supervisors do not know how to do the project. That is what he hired you to find out. Tell him/her what you need (can you read THIS?; does A, B or C make more sense to do? (add your own thoughts here); who do I need to talk to about X? (+ So do I or you send an email about that? If you are not comfortable say it)). Your supervisor is likely the worst "mind reader" you have ever met but hopefully one of the people most willing to open doors for you. But you have to say what you need.
* Professors are often a species that thrive in high chaos environments. Don't let their stress become yours because a PhD student is another species that generally require order to be productive and survive.
* When dealing with your professor try for you to be the one that provide order and then let him/her provide the wishdom. That is true for your project as well as organizing anything your professor asks you to do.
* You signed up for a project with this great professor as your supervisor. But you should realise that he likely has many students and a million other things to do. It doesn't matter that he is great. On a daily basis he won't be the person most useful to you. Make sure to find lower level people (the hungry assistant professors and postdocs) to involve in your project/supervision.
* Article on team mentoring: http://science.sciencemag.org/content/359/6374/486

## Getting help
* Before you generate any data read: http://www.tandfonline.com/doi/full/10.1080/00031305.2017.1375989. If you do not the person you ask for help with data analysis will despice you. Case study: https://twitter.com/JanStanstrup/status/910511291898433537
* As a general rule don't show up and say "I don't know how to do X". Come and say "I am having trouble with X. I looked here and there and have thought I could do A, B, or C but I am not sure how to choose." Remember that the goal is not making you solve X as fast as possible. It is helping you to become a person that can solve X independently.
* In the hopefully rare event that you are completely lost don't waste a week wondering. Identify who can help you instead. Your thought process should be: have I done what I could to figure this out? Is there other places I could look? Other things I could try? That said you should also think: How long would it take me to figure this vs. how long will it take for the guy in the next room to point me in the right direction? If you would take a week to figure something while someone can point you in the right direction in 5 minutes then ASK NOW.
* No, your professor does not remember what you told him last week. Put things in context when you ask advise and don't assume that he/she remembers the context.
* The usefulness of the advise you get is propertional to the amount of context you give. If you show up friday and ask a postdoc about a problem you have you are unlikely to get more insight than what you had yourself. Postdocs and others might have more experience than you but they are not magic genies. If your data doesn't make sense don't throw it on the table of someone and expect them to figure out what the problem is. If you couldn't figure it out it is likely not so trivial. Instead send an email explaining the problem (what did you do, what did you expect, what do you see). Attach the data and/or tell them where to find it. This way they can look calmly when they have time and try to understand what is going on. If you are there it suggest they should be able to come up with an answer on the spot while you are there. This leads to a superficial look unlikely to help you.
* Stack your meetings. Having space between meetings is wasted time because you cannot start working very focussed if you have short time. So rather one full day of meetings than one each day. This is especially important for you supervisor! If you see that have one day all clear then do not book a meeting there if possible! That is their time to actually work! So rather fill the holes they have between other meetings.

## Writing papers
* Many comments/corrections are usually a good thing. That means that the one giving feedback thinks it is at a level where it is worth dealing with details.
* If you cannot outline in the margin of your paper with 1 to 2 words what each paragraph is about then you do not have a clear enough train of thought. You are likely mixing different concepts and jumping back and forth between subjects.

## Conferences
* If you are young and scared to talk to more senior people at a conference try to catch who you want to talk to at the poster session. Ask questions. People in academia usually love to explain things (that is why they like science) and like to talk about what they have done (they have spend time making that damn poster about something). That is your way in to ask about something else if you want to.
If someone seem annoyed talking to you in this context you can be pretty sure they are arogant assholes not worth your time or admiration.
* Don't feel like you *have* go to *every* conference session if there is not a reason you want to be there. Take breaks in conference. Otherwise you burn out very fast and then you won't retain anything. Get a hotel close, so you can recharge for some time if you need to.


More here:
* https://twitter.com/Stylish_Academi/status/785188271643123712
* http://www.opiniomics.org/tips-for-phd-students-and-early-career-researchers/
* https://twitter.com/thehauer/status/1021179403680862218?s=03
* https://www.pnas.org/content/116/42/20910

## Don'ts
* Do not use images from the internet in any public material unless you are 100 % sure they are public domain or you hold the copyright: https://kunet.ku.dk/nyhedsrum/nyheder/Sider/Et-enkelt-googlet-billede-kan-koste-dyrt.aspx?utm_campaign=773258_Nyhedsbrev_fra_NEXS_2019,_Uge_32&utm_medium=email&utm_source=Main+list+-+Live+data-All_Users + https://kunet.ku.dk/nyhedsrum/nyheder/Sider/Vide-rammer-for-brug-af-billeder-til-undervisning.aspx?utm_campaign=776638_Nyhedsbrev_fra_NEXS_2019,_Uge_34&utm_medium=email&utm_source=Main+list+-+Live+data-All_Users

 

