Dear XXXX,



Thank you for your interested in working with me and the group. What follows is a standard reply to unsolicited applications for PhDs. This is meant to inform you about the realities of applying for a PhD in Denmark and to give you general pointers as to why you didn't receive an enthusiastic reply to your request. These are things you need to understand before we can go any further.

The first thing to understand is that in Denmark there is almost no basic funding. That means that only the full professors and some associate professors have salaries covered by the university. All other funding comes directly from specific research projects normally lasting 1-5 years. This means that I, and most anyone else, do not have funding they can use to hire you as a PhD student; no matter how fantastic your CV is. In all honesty all "loose" money like that are usually used for anything else than PhDs; lab techs, assistant professors etc. that can be difficult to get other funding for. Professors are flooded with requests with PhD; so much so that most are not even read.

So, for these reason approaching someone basically saying "I would like to do a PhD with you; what can you offer?" will never be successful when you are unknown to me/us. If the ideas come from me/us they will go into a grant application, which if successful will result in a posted position. All available positions are posted here: http://employment.ku.dk/phd.

Therefore, an email that just hints at mass-emailing is sure to land your email in the bin. That won't be taken seriously. I know finding a PhD can be frustrating but the only way you could hope to get one by contacting people is if A) There is a clear reason why you are contacting this particular person/group and not someone else. B) You have a more or less clear idea of what you would like to work with. C) You have more than strong grades and vague familiarity with the techniques we use or subject we study.

However, there are a few inside tracks. This is basically what you are asking when you emailed me. You need to be aware of that. And you need to be very clear on why you are exactly the person exactly this group needs.



**Option A: You have a specific idea that you think would fit in the group**

This inside track would be what we would try with an exceptional student that for example has done a master project in the group. We might work with such a student to craft an application targeted for this student and his/her interests. As you are not known to us you are already an a huge disadvantage here. 

This would require that you have a relatively specific idea of what you would like to do. Otherwise, as I said, we would just post the position. To do this you need to try to understand what this group does, who is in it, what area you would like to work in. Not as in "I would like to do metabolomics"/"I am good with an MS"/"I love chemistry"/"I find the environment important"; but much more specific than that.



**Option B: You have an attractive funding source in mind**

Here you would have identified a source of funding appropriate for the group you are approaching but you need a supervisor. If this is the case your email should include details about the funding source (what does it cover? only your salary? costs? how much? overhead?), deadlines, if you have other collaborators in mind.



**Option C: You already have a scholarship**

Congratulations! That is great. BUT... Again. Why exactly this group? That your grant requires you to go abroad and that you have a degree in chemistry is not specific enough. You still need to target your email to  pique an interest.

Another thing to make very clear is what your scholarship covers. Does it only cover your salary? If so that makes it difficult to accept as we will need to find funds to actually run your project. This can be possible if you are just the right person at the right time.

Be aware that some scholarships are very hard to live on in Denmark. Denmark is expensive. For example, the grants the China Scholarship Council gives out are equivalent to roughly half the salary a Danish PhD student gets. Unless you have other financial help, you would be living at a minimum level which the university does not allow, so other funding will need to be found.


I hope this has been remotely helpful. Good luck!
